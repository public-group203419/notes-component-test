import "./style.scss";

import React, { useState, useEffect } from "react";
import { TextField, Button, Grid, Paper, Container, Box } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import Masonry from "@mui/lab/Masonry";
import { styled } from "@mui/material/styles";
import TableComponent from './TableComponent';

const colors = [
  "#B9F6CA",
  "#ffdede",
  "#CFD8DC",
  "#FFE0B2",
  "#EF9A9A",
  "#F9FBE7",
  "#90CAF9",
  "#C5CAE9",
  "#E1F5FE",
  "#E1BEE7 ",
];
const NotesComponent = () => {
  const [notes, setNotes] = useState([]);
  const [validation, setValidation] = useState(false);
  const [submitData, setSubmitData] = useState(false);
  const [currentNote, setCurrentNote] = useState({ title: "", content: "" });
  const [editIndex, setEditIndex] = useState(null);
  useEffect(() => {
    const notesLocal = localStorage.getItem("notes");
    if (notesLocal) {
      setNotes(JSON.parse(notesLocal));
    }
  }, []); 
  const addNote = () => {
    setSubmitData(true);
    if (currentNote.title.trim() !== "" && currentNote.content.trim() !== "") {
      setValidation(false)
      setSubmitData(false)
      if (editIndex !== null) {
        // Edit existing note
        const updatedNotes = [...notes];
        updatedNotes[editIndex] = currentNote;
        setNotes(updatedNotes);
        localStorage.setItem("notes", JSON.stringify(updatedNotes));
        setCurrentNote({ title: "", content: "" });
        setEditIndex(null);
      } else {
        // Add new note
        localStorage.setItem("notes", JSON.stringify([...notes, currentNote]));
        setNotes([...notes, currentNote]);
        setCurrentNote({ title: "", content: "" });
      }
    } else {
      setValidation(true);
    }
  };

  const deleteNote = (index) => {
    const updatedNotes = [...notes];
    updatedNotes.splice(index, 1);
    setNotes(updatedNotes);
    localStorage.setItem("notes", JSON.stringify(updatedNotes));
  };

  const editNote = (index) => {
    setCurrentNote(notes[index]);
    setEditIndex(index);
  };

  return (
    <div>
      <Container fixed>
        <h1 className="text-left">Notes</h1>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12}>
            <Paper elevation={3}>
              <TextField
                label="Title"
                variant="outlined"
                fullWidth
                value={currentNote.title}
                onChange={(e) =>
                  setCurrentNote((prevNote) => ({
                    ...prevNote,
                    title: e.target.value,
                  }))
                }
              />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper elevation={3}>
              <TextField
                label="Content"
                variant="outlined"
                fullWidth
                multiline
                rows={4}
                value={currentNote.content}
                onChange={(e) =>
                  setCurrentNote((prevNote) => ({
                    ...prevNote,
                    content: e.target.value,
                  }))
                }
              />
            </Paper>
          </Grid>
        </Grid>
        {submitData && validation && <span className="errorText">All fileds are required !!</span>}
        <Box align="right">
         
          <Button
            variant="contained"
            onClick={addNote}
            style={{ marginTop: "15px", marginBottom: "35px" }}
          >
            {editIndex !== null ? "Update Notes" : "Add Notes"}
          </Button>
        </Box>

        <Box>
        <TableComponent data={notes} deleteNote={deleteNote} addNote={addNote} editNote={editNote}/> 
        </Box>
      </Container>
    </div>
  );
};

export default NotesComponent;
