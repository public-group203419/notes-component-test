import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TextField,
  TableSortLabel,
  Button,
  Box,
} from "@mui/material/";

import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import { styled } from "@mui/material/styles";

const useStyles = styled((theme) => ({
  container: {
    marginBottom: theme.spacing(2),
  },
  searchField: {
    marginBottom: theme.spacing(2),
  },
}));

const DataTable = ({ data, deleteNote, editNote }) => {
  console.log(data, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
  const classes = useStyles();
  const [searchText, setSearchText] = useState("");
  const [sortedColumn, setSortedColumn] = useState(null);
  const [sortOrder, setSortOrder] = useState("asc");
  const [viewDoteData, setViewDoteData] = useState("na");

  const viewNote = (event) => {
    setViewDoteData(event);
  };
  const handleSearch = (event) => {
    setSearchText(event.target.value);
  };

  const handleSort = (column) => {
    if (column === sortedColumn) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortedColumn(column);
      setSortOrder("asc");
    }
  };

  const filteredData = data.filter(
    (item) =>
      item.title.toLowerCase().includes(searchText.toLowerCase()) ||
      item.content.toLowerCase().includes(searchText.toLowerCase())
  );

  const sortedData = filteredData.sort((a, b) => {
    if (sortOrder === "asc") {
      return a[sortedColumn] < b[sortedColumn] ? -1 : 1;
    } else {
      return a[sortedColumn] > b[sortedColumn] ? -1 : 1;
    }
  });

  return (
    <div className="notesTable">
        <Box className="tableSearch">
      <TextField
        className={classes.searchField}
        label="Search"
        value={searchText}
        onChange={handleSearch}
      /></Box>
    
      <TableContainer component={Paper} className={classes.container}>
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow>
              <TableCell>
                <TableSortLabel
                  active={sortedColumn === "title"}
                  direction={sortOrder}
                  onClick={() => handleSort("title")}
                >
                  Title
                </TableSortLabel>
              </TableCell>
              <TableCell>
                <TableSortLabel
                  active={sortedColumn === "content"}
                  direction={sortOrder}
                  onClick={() => handleSort("content")}
                >
                  Body
                </TableSortLabel>
              </TableCell>
              <TableCell className="actionLabel">
                <TableSortLabel>Action</TableSortLabel>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedData.length === 0 && <p className="notDatatxt">No Data found !</p>}
            {sortedData.map((item, index) => (
              <TableRow key={index}>
                <TableCell className="titleContent">{item.title}</TableCell>
                <TableCell>
                  {viewDoteData == index && <p className="fullContent">{item.content}</p>}
                  {viewDoteData != index && <p className="shotContent">{item.content}</p>}
                </TableCell>
                <TableCell className="actionBtn">
                  <Button
                    className="iconButton viewNote"
                    variant="text"
                    onClick={() => viewNote(index)}
                  >
                    <RemoveRedEyeIcon></RemoveRedEyeIcon>
                  </Button>
                  <Button
                    className="iconButton"
                    variant="text"
                    onClick={() => editNote(index)}
                  >
                    <EditIcon></EditIcon>
                  </Button>
                  <Button
                    className="iconButton"
                    variant="text"
                    onClick={() => deleteNote(index)}
                  >
                    <DeleteIcon></DeleteIcon>
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default DataTable;
